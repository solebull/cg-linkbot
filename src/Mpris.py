
import dbus

# A MPRIS specification to handle media player next command
# It will handle minimal audio playlist control through dbus
# see specification https://specifications.freedesktop.org/mpris-spec/latest/

#DBUS_DEST="org.gnome.Rhythmbox3"    # On gnome
DBUS_DEST="org.mpris.MediaPlayer2.vvave" # See specifications for possible apps
DBUS_PATH="/org/mpris/MediaPlayer2" # Dbus path to use (available commands)
DBUS_IFACE="org.mpris.MediaPlayer2.Player" # Dbus interface name

class Player:
    """
    Handle the next() command for a dbus-ready media player implementing
    the MPRIS specification. For more information, see 
    https://specifications.freedesktop.org/mpris-spec/latest/

    If rhythmbox (or specified destination application) is not started,
    will start it. Will not start playing any playlist.

    To navigate through the local dbus services, you may want to test for
    `d-feet` (sudo apt install d-feet on debian-based distributions).
    """
    player=None
    def __init__(self):
        """Establish the connection to dbus object"""
        session_bus = dbus.SessionBus()
        self.proxy_obj = session_bus.get_object(DBUS_DEST, DBUS_PATH)
        self.player = dbus.Interface(self.proxy_obj, DBUS_IFACE)

    def status(self):
        """
        return Playing||Stopped (or others according to the player's 
        status. Mainly an example on how to retrieve dbus properties.
        """
        iface=dbus.Interface(self.proxy_obj, 'org.freedesktop.DBus.Properties')
        return iface.Get('org.mpris.MediaPlayer2.Player', 'PlaybackStatus')
        
    def next(self, author):
        """Skip the current song."""
        cgn=self.player.CanGoNext
        if self.status()=='Playing':
            self.player.Next()
            return "Next (%s)" % author
        else:
            return "Not running (%s)" % author
            
