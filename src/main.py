#!/usr/bin/python3

import Browser
import Mpris
import configparser

import secrets
from twitchio.ext import commands

import inspect # For test purpose only

class Bot(commands.Bot):
    """
    The real twitchio-based bot class. Holds internal objects used to
    answer chatters commands.
    """
    player=None  # The dbus object to mediaplayer
    def __init__(self):
        """
        Initialise our Bot with our access token, prefix and a list 
        of channels to join on boot...
        """
        super().__init__(token=secrets.TMI_TOKEN, prefix=secrets.BOT_PREFIX,
                         initial_channels=[secrets.CHANNEL])
        self.player=Mpris.Player()
        self.ff=Browser.BrowserFirefox()

    def has_args(self, ctx):
        """
        Should return True if given command.Context has a param.
        """
        return True
        
    async def event_ready(self):
        """We are logged in and ready to chat and use commands..."""
        print(f"\nLogged in as | {self.nick}")

    # Add your custom commands below this line ----------------------
        
    @commands.command()
    async def next(self, ctx: commands.Context):
        await ctx.send(self.player.next(ctx.author.name))

    @commands.command()
    async def link(self, ctx: commands.Context):
        url=str(self.ff.getClashUrl())
        print("Firefox.getClashURL returns:'"+url + "'")
        await ctx.send("%s (%s)" % (url, ctx.author.name))

    @commands.command()
    async def sar(self, ctx: commands.Context):
        """
        We read the code from text file each and every time someone issue the
        command and avoid restarting the bot when the SAR code change.
        So, keep reading this file each time and do not bufferize its value.
        """
        config = configparser.ConfigParser()
        config.read('../codes.ini')
        code=config['DEFAULT']['sar']
        await ctx.send("cross-platform code: %s (%s)"%(code, ctx.author.name))

    @commands.command()
    async def mtga(self, ctx: commands.Context):
        """
        We read the code from text file each and every time someone issue the
        command and avoid restarting the bot when the SAR code change.
        So, keep reading this file each time and do not bufferize its value.
        """
        config = configparser.ConfigParser()
        config.read('../codes.ini')
        code=config['DEFAULT']['mtga']
        await ctx.send("MTGA username: %s (%s)"%(code, ctx.author.name))

    @commands.command()
    async def post(self, ctx: commands.Context):
        """
        A user try to post a link, validated from a whitelist.
        """
        if self.has_args(ctx):
            #print(inspect.getmembers(ctx.message))
            print(inspect.getmembers(ctx.command))
            print(ctx.command.params)
        else:
            print("Usage: !post [link]")
        
if __name__ == "__main__":
    #run()
    bot = Bot()
    bot.run()
