import os
from os.path import expanduser
import glob
import json
import re
import time
from time import sleep

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler

def p(text):
    "Print without newline and immediately flush"
    print(text, end='', flush=True)

def report(url):
    """
    Extract the report id of clash of code from its URL. 
    Returns None if not a CoC
    """
    clash=re.compile(r'clashofcode/clash/report/([0-9a-fA-F]+)$')
    m=re.search(clash,url)
    if m:
        return m.group(1)
    
def id(url):
    "Extract the id of clash of code from its URL. Returns None if not a CoC"
    clash=re.compile(r'clashofcode/clash/([0-9a-fA-F]+)$')
    m=re.search(clash,url)
    if m:
        return m.group(1)
    else:
        return report(url)
    
class Event(LoggingEventHandler):
    """
    This class handles the event handler from watchdog and listen to events
    from the complete session store directory.
    """
    file   =None     # The file the event handler is monitoring
    browser=None     # The browser object
    ltime  =None     # Last modification time
    lurl   =None     # Last url to know if it changed
    
    def __init__(self, vFile, browser):
        """
        browser: We must pass the complete Browser object to have
        access to the getClashUrl() function.
        """
        super().__init__()
        self.file=vFile
        self.browser=browser
        print("Watching for " + vFile)
        self.on_modified(None)

    def on_modified(self, event):
        """
        Call whenever the session store directory is modified. The way firefox
        handle this file is strange. It's working on a .bak file first then
        moves/copy this file under the sessionstore name.
        """
        url=self.browser.extractClashUrl()
        if url:
            epoch = int(time.time())
            if epoch!=self.ltime:
                self.ltime=epoch
                if url!=self.lurl:
                    self.lurl=url
                    print("\n[{}] - modified: {}".format(time.asctime(), url))
                else:
                    p(".")
        else:
            p('.')

class Browser:
    'The base class for url-getting and browser-based action'
    def extractClashUrl(self):
        'Should return the current Clash of Code URL.'
        pass
    def getClashUrl(self):
        'Should return the current Clash of Code URL.'
        pass

class BrowserFirefox(Browser):
    'The firefox specialization'
    
    home=None          # The current user's home directory
    ffpaths=None       # Contains all firefox sessions paths
    event_handler=None # The watchdog observer's event handler
    
    def __init__(self):
        self.home=str(expanduser("~")) #/.mozilla/firefox/"))
        self.ffpaths=glob.glob(self.home+'/.mozilla/firefox/*.default-release/sessionstore-backups/recovery.jsonlz4')
        self.ffpath=os.path.dirname(self.ffpaths[0])
        if not self.ffpath:
            raise Exception("Can't find Firefox session file. Please be sure FF is running and restart.")

        # Watchdog integration (see https://stackoverflow.com/a/32923569)
        print("Starting watchdog observer on %s"  % self.ffpath)
        self.event_handler = Event(self.ffpath, self)
        observer = Observer()
        observer.schedule(self.event_handler, self.ffpath, recursive=False)
        observer.start()
        
    def mozlz4_to_text(self, filepath):
        "Return uncompressed text from a 'mozlz4', 'jsonlz4', 'baklz4' file"
        import lz4.block
        if os.path.isfile(filepath):
            bytestream = open(filepath, "rb")
            bytestream.read(8)  # skip past the b"mozLz40\0" header
            valid_bytes = bytestream.read()
            text = lz4.block.decompress(valid_bytes)
            return text
        else:
            p('N')       # Not a file either (but not a directory)
            return ""

    def extractClashUrl(self):
        "Must return the current ClashOfCode URL"
        windows=[]
        for file in self.ffpaths:
            "Find all windows from session store"
            try:
                js=self.mozlz4_to_text(file)
                if js:
                    jscontent=json.loads(js)['windows']
                    windows.append(jscontent)
            except FileNotFoundError:
                p('N')       # It's a directory

        for w in windows:
            for w2 in w:
                "Search for tabs"
                for t in w2['tabs']:
                    # Because entries is the tab's history. See
                    # https://wiki.mozilla.org/Firefox/session_restore+
                    #   #The_structure_of_sessionstore.js for more
                    url=t['entries'][-1]['url']
                    if re.search('clashofcode/clash', url):
                        return url
        return None

    def getClashUrl(self):
        return self.event_handler.lurl
