# cg-linkbot

[[_TOC_]]

A python-powered twitch bot to post current **codingame.com**'s 
*clash of code* link.

## Dependencies

You have to install this package in order to install `dbus-python` module :

	sudo apt install libdbus-1-dev

Then, you can install python-based dependencies :

	pip install -r requirements.txt

## Running

	cd src
	./main.py

## Unit tests

	make check

## Secrets file

`src/secrets.py` file should contain these keys :
	
	TMI_TOKEN='oauth:mklmlkmlkml'
	BOT_NICK='mybot'
	BOT_PREFIX='!'
	CHANNEL='channel_to_join_at_startup'

For the *TMI_TOKEN*, you have to register to https://twitchapps.com/tmi/ .
The bot may need to be moderator to post links in the twitch chat.

## Commands

	!next : start to play to next song immediately;
	!link : get the HTTPS link to the current codingame.com Clash of Code;
	!sar  : prints an arbitrary code for the Super Animal Royale game contained
	    in top-level sar-code.txt file;

The *sar* code can b found and modified in src/main.js.

## Troubleshooting

You can't run the bot if no instance of firefox is running. It will end 
prematurely with a `IndexError: list index out of range` python error. To fix
it, please launch firefox **before** running the bot.
