#!/usr/bin/python3

import unittest

from Browser import id

class TestBrowser(unittest.TestCase):
    pass
    def test_id(self):
        "Test that we can extract a clash ID from its URL"
        vid='19361116dc048fdb6a2a05be89324c08c1e9b82'
        url=' https://www.codingame.com/clashofcode/clash/'+vid
        self.assertEqual(id(url), vid)

    def test_report(self):
        """
        Test that we can extract a clash ID from its report URL.
        It uses the id() function but also tests its report() one.
        """
        vid='19361116dc048fdb6a2a05be89324c08c1e9b29'
        url=' https://www.codingame.com/clashofcode/clash/report/'+vid
        self.assertEqual(id(url), vid)

    def test_ide(self):
        """
        Test that we can't extract ID, we return None. Especially with 
        ide-style URL.
        """
        notanid='42171027cd00adb911156503e4c9c1bcbfe9c6e9'
        url=' https://www.codingame.com/ide/'+notanid
        self.assertEqual(id(url), None)
        self.assertNotEqual(id(url), notanid)
        

"""
    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)
"""
if __name__ == '__main__':
    unittest.main()
